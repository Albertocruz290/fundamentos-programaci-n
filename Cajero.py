#coding: utf-8
"""
    Programa para simular un cajero automatico
    de un banco
"""

#seleccion  de variables
saldo_minimo = 4000
retiro_maximo = 8000


def  apertura_cuenta(apc):
    if apc < 4000:
        return"La Cantidad Minima es $4000"
    else:
        return apc
        
def depositar(deposito):
    return deposito
    
def  pago_servicio(saldo, pago):
    if saldo <= pago:
        return True
    else:
        return False
        

def menu():
    i = 1
    while i == 1:
        print("0: Abrir Cuenta")
        print("1: Retirar")
        print("2: Depositar")
        print("3: Consultar")
        print("4: Pagar Servicio")
        print("5: Salir")
        
        opc = int(input("Elegir una Opcion: "))


        if opc == 5:
            i = 2
        elif opc == 0:
            saldo = apertura_cuenta(int(input("La Apertura de Cuenta es con 4000. ")))
            print("Tu Saldo Actual es: ${0}", format(saldo))
        elif opc == 1:
            pass
        
        elif opc == 2:
            saldo += depositar(int(input("Escriba la Cantidad a Depositar")))
            print("Tu Saldo Actual es: ${0}", format(saldo))
            
        elif opc == 3:
            print("Tu Saldo Actual es: ${0}", format(saldo))
            
        elif opc == 4:
            pago = int(input("Escriba la Cantidad a Pagar: --> "))
            if pago_servicio(saldo, pago):
                print("Saldo Insuficiente")

        else:
            print("Opcion Invalida")

menu()




