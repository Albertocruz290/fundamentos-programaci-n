#include <iostream>
#include <stdio.h>

using namespace std;

void gotoxy(int x, int y) {
    printf("\x1b[%d;%df", y, x);
}


int main() {

  int matrix[3][3];
  string jugador1;
  string jugador2;
  int x , y;

	system("clear");

  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {

      matrix[i][j] = 2;
    }
  }

	for (int i = 0; i < 20; ++i)
	{
		/* code */
		gotoxy(10, i);
		printf("|");

		gotoxy(30, i);
		printf("|");


	}

	for (int i = 0; i < 50; ++i)
	{
		/* code */
		gotoxy(i, 5);
		printf("-");

		gotoxy(i, 15);
		printf("-");
	}


	gotoxy(5, 10);
	printf("X");

	gotoxy(20, 10);
	printf("O");
	//system("PAUSE()");

  for (int i = 1; i <= 9; ++i) {
    if (i%2 ==0) {
      cout<<"Turno del Jugador 2 "<<jugador2<<endl;
      cout<<"Posicion para X: "<<endl; cin>>x;
      cout<<"Posicion para Y: "<<endl; cin>>y;
      gotoxy(x,y);
      matrix[x][y] = 0;
      if (matrix[0][0] == 0 && matrix[0][1] == 0 && matrix[0][2] == 0) {
        cout<<"Jugador2 es el Ganador "<<endl;
        break;
      }
      cout<<"J2"<<endl;
    }
    else {
      cout<<"Turno del Jugador 1 "<<jugador1<<endl;
      cout<<"Posicion para X: "<<endl; cin>>x;
      cout<<"Posicion para Y: "<<endl; cin>>y;
      gotoxy(x,y);
      matrix[x][y] = 1;
      if (matrix[0][0] == 0 && matrix[0][1] == 0 && matrix[0][2] == 0) {
        cout<<"Jugador1 es el Ganador "<<endl;
        break;
      cout<<"J1"<<endl;
    }
  }

  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {

      cout<<matrix[i][j]<<endl;
    }
  }

	getchar();
	return 0;
}
